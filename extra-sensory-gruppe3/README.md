# Anleitung zur Ausführung des Programms
* Wir gehen in jedem Schritt davon aus, dass wir uns direkt im Verzeichnis extra-sensory-gruppe3 befinden.
* Außerdem ist Python 3.6+ erforderlich

## Setup Python
* pip install -r requirements.txt

## train.py
* python train.py **D:\Cache\ExtraSensory.per_uuid_features_labels** estimators
* Dabei ist **D:\Cache\ExtraSensory.per_uuid_features_labels** in diesem Fall das Verzeichnis, 
  in dem die entpackten .csv-Dateien mit UUID im Namen liegen
* Und estimators **(ohne trailing "/")** ist der relative Pfad zum Ausführungsverzeichnis.
* Wenn es irgendwie geht, sollte auch tatsächlich nur der Pfad zu den csv-Dateien im obigen Beispielkommando geändert
  werden, denn dann können die generierten Classifier auch direkt in unserem Notebook verwendet werden.
* Alternativ kann die Umgebungsvariable TASK_TYPE auf CPU gesetzt werden, um zu verhindern, dass CUDA verwendet wird:
  **export TASK_TYPE="CPU"** (Der Default nutzt CPU)
  
## predict.py
* python predict.py D:\Cache\ExtraSensory.per_uuid_features_labels estimators **output/output.csv**
* Dabei ist **output/output.csv** in diesem Fall der relative Pfad zur Ausgabedatei.
  Auch hier wäre wünschenswert, wenn dieser beibehalten wird ;-)

## Warnungen
* Aus Zeitgründen wurden Argumentvalidierungen nicht eingebaut. 
  Bitte daher vorallem darauf achten, dass am Ende von Verzeichnisnamen **kein** / steht. 
  Außerdem bitte derzeit (lieber :D) nicht die Ausgaben von train.py in das Wurzelverzeichnis schreiben lassen.
* Ganz sicher sind wir unterwegs, wenn einfach in den Commands oben nur der absolute Pfad zum Dataset geändert wird :p

## Unser Notebook mit Vergleichen zum originalen Paper
Ist auch in diesem Projekt enthalten (main.ipynb) aber GitLab unterstützt ohne weiteres Setup eine 
Notebook-Visualisierung:
[Hier geht's zum Notebook](https://gitlab.com/hakenadu91/extra-sensory/blob/master/extra-sensory-gruppe3/main.ipynb)

## PIP FREEZE
catboost==0.17  
cycler==0.10.0  
dill==0.3.0  
graphviz==0.13  
joblib==0.13.2  
kiwisolver==1.1.0  
matplotlib==3.1.1  
numpy==1.17.2  
pandas==0.25.1  
plotly==4.1.1  
pyparsing==2.4.2  
python-dateutil==2.8.0  
pytz==2019.2  
retrying==1.3.3  
scikit-learn==0.21.3  
scipy==1.3.1  
six==1.12.0