# -*- coding: utf-8 -*-

user_id_label_name = "label:user"

ignore = [
    "timestamp",
    "label_source"
]

main_activity_label_name = "label:main_activity"
main_activity_missing_label_name = "label:main_activity:none"
main_activity_label_names = [
    "label:LYING_DOWN",
    "label:SITTING",
    "label:OR_standing",
    "label:FIX_running",
    "label:FIX_walking",
    "label:BICYCLING"
]

phone_label_name = "label:phone"
phone_missing_label_name = "label:phone:none"
phone_label_names = [
    'label:PHONE_IN_POCKET',
    'label:PHONE_IN_HAND',
    'label:PHONE_IN_BAG',
    'label:PHONE_ON_TABLE'
]

various_a_label_name = "label:various_a"
various_a_missing_label_name = "label:various_a:none"
various_a_label_names = [
    'label:IN_CLASS',
    'label:OR_outside',
    'label:BATHING_-_SHOWER',
    'label:DOING_LAUNDRY',
    'label:AT_A_BAR',
    'label:AT_THE_GYM'
]

various_b_label_name = "label:various_b"
various_b_missing_label_name = "label:various_b:none"
various_b_label_names = [
    'label:SLEEPING',
    'label:LAB_WORK',
    'label:IN_A_MEETING',
    'label:IN_A_CAR',
    'label:FIX_restaurant',
    'label:OR_exercise',
    'label:COOKING',
    'label:AT_A_PARTY',
    'label:ELEVATOR'
]

aggregated_column_names = [
    user_id_label_name,
    main_activity_label_name,
    various_a_label_name,
    various_b_label_name,
    phone_label_name
]

global_random_state = 1337
