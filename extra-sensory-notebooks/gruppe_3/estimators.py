from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from catboost import Pool
import pandas as pd


class CatBoostEarlyStoppingEstimator(BaseEstimator, ClassifierMixin):

    def __init__(self,
                 classifier,
                 cat_features,
                 verbose_eval=200):
        super().__init__()
        self.classifier = classifier
        self.cat_features = cat_features
        self.verbose_eval = verbose_eval

    def fit(self, X, y, X_val=None, y_val=None):
        eval_set = None
        if X_val is None and y_val is None:
            eval_set = Pool(X_val, y_val, cat_features=self.cat_features)
        return self.classifier.fit(X, y,
                                   cat_features=self.cat_features,
                                   verbose_eval=self.verbose_eval,
                                   eval_set=eval_set)

    def predict(self, X):
        return self.classifier.predict(X)

    def predict_proba(self, X):
        return self.classifier.predict_proba(X)


class FilteredPipeline(Pipeline):

    def __init__(self, steps, y_filter):
        super().__init__(steps)
        self.y_filter = y_filter

    def fit(self, X, y=None, X_val=None, y_val=None):
        if X_val is not None and y_val is not None:
            return super().fit(X, self.y_filter(y), {
                "classify_X_val": X_val,
                "classify_y_val": self.y_filter(y_val)
            })
        return super().fit(X, self.y_filter(y))


class PredictorChain(BaseEstimator, ClassifierMixin):
    """Verkettung der einzelnen Classifier (pandas-basiert)

    Alle Classifier werden mit allen Labels aufgerufen.
    Jeder Classifier muss selbst dafür sorgen, dass er eine Teilmenge der Labels selbst wieder als Features verwendet.
    """

    def __init__(self, classifiers):
        self.classifiers = classifiers

    def __get_classifier_prediction(self, classifier, X):
        """Liefert Ergebnis von predict_proba oder predict (ersteres wird bevorzugt, sofern vorhanden)

        :param classifier: Classification Estimator, welcher inferiert wird
        :param X: Features
        :return: y_pred
        """

        if hasattr(classifier, "predict_proba_pd"):
            return classifier.predict_proba_pd(X)
        else:
            return classifier.predict(X)

    def fit(self, X, y, X_val, y_val):
        for classifier in self.classifiers:
            classifier.fit(X, y, X_val, y_val)
            X = pd.concat([X, self.__get_classifier_prediction(classifier, X)], axis=1)
            if X_val is not None:
                X_val = pd.concat([X_val, self.__get_classifier_prediction(classifier, X_val)], axis=1)
        return self

    def predict(self, X):
        y_pred = None

        for classifier in self.classifiers:
            y_iteration = self.__get_classifier_prediction(classifier, X)
            X = pd.concat([X, y_iteration], axis=1)
            if y_pred is None:
                y_pred = y_iteration
            else:
                y_pred = pd.concat([y_pred, y_iteration], axis=1)

        return y_pred
