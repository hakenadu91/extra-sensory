import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.impute import SimpleImputer
from catboost import CatBoostClassifier

from keras.wrappers.scikit_learn import KerasClassifier
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Dense, Dropout

from gruppe_3.constants import global_random_state, user_id_label_name, main_activity_label_name
from gruppe_3.estimators import FilteredPipeline, CatBoostEarlyStoppingEstimator

import tensorflow as tf
from keras.backend import set_session
import random
import numpy as np

random.seed(global_random_state)
np.random.seed(global_random_state)
tf.set_random_seed(global_random_state)

config = tf.ConfigProto(allow_soft_placement=True)
set_session(tf.Session(config=config))


class UserClassifier(FilteredPipeline):
    """Classifier für das User-Label (Zusatzaufgabe)

    Ein Schritt der Klassifizierungskette.
    Pipeline-Spezifikation, welche in der Lage ist, anhand der reinen Features vorherzusagen, um welchen User
    es sich handelt.
    """

    def __init__(self, cat_features):
        super().__init__(
            steps=[
                ("classify", CatBoostEarlyStoppingEstimator(
                    CatBoostClassifier(
                        task_type="GPU",
                        random_state=global_random_state,
                        depth=8,
                        iterations=10000,
                        l2_leaf_reg=2.0,
                        early_stopping_rounds=200,
                        verbose=10
                    ),
                    cat_features=cat_features,
                    verbose_eval=100
                ))
            ],
            y_filter=lambda y: y[user_id_label_name]
        )

    def predict_proba_pd(self, X):
        return pd.DataFrame(self.predict_proba(X), index=X.index).add_prefix("proba:user_").fillna(0)


class MainActivityClassifier(FilteredPipeline):
    """Classifier für die Main-Activity-Labels

    """

    def __init__(self):
        super().__init__(
            steps=[
                ("impute", SimpleImputer()),
                ("standardize", StandardScaler()),
                ("classify", CatBoostClassifier(
                    task_type="GPU",
                    random_state=global_random_state,
                    depth=8,
                    iterations=50,
                    verbose=10
                ))
            ],
            y_filter=lambda y: y[main_activity_label_name]
        )

    def predict_proba_pd(self, X):
        return pd.DataFrame(self.predict_proba(X), index=X.index).add_prefix("proba:main_activity_").fillna(0)


class PhoneSecondaryActivityClassifier(FilteredPipeline):
    def __init__(self):
        super().__init__(
            steps=[
                ("impute", SimpleImputer()),
                ("standardize", StandardScaler()),
                ("classify", CatBoostClassifier(
                    verbose=10,
                    task_type="GPU",
                    iterations=2500,
                    l2_leaf_reg=3.0,
                    loss_function="MultiClass",
                    depth=7,
                    random_state=42
                ))
            ],
            y_filter=lambda y: y[main_activity_label_name]
        )

    def predict_proba_pd(self, X):
        return pd.DataFrame(self.predict_proba(X), index=X.index).add_prefix("proba:phone_").fillna(0)


class VariousMutexClassifierA(FilteredPipeline):
    def __init__(self):
        super().__init__(
            steps=[
                ("impute", SimpleImputer()),
                ("standardize", StandardScaler()),
                ("classify", CatBoostEarlyStoppingEstimator(
                    CatBoostClassifier(
                        verbose=10,
                        task_type="GPU",
                        iterations=10000,
                        early_stopping_rounds=200,
                        l2_leaf_reg=3.0,
                        loss_function="MultiClass",
                        depth=6
                    )
                ))
            ],
            y_filter=lambda y: y[main_activity_label_name]
        )

    def predict_proba_pd(self, X):
        return pd.DataFrame(self.predict_proba(X), index=X.index).add_prefix("proba:various_a_").fillna(0)


class VariousMutexClassifierB(FilteredPipeline):
    def __init__(self):
        super().__init__(
            steps=[
                ("impute", SimpleImputer()),
                ("standardize", StandardScaler()),
                ("classify", CatBoostEarlyStoppingEstimator(
                    CatBoostClassifier(
                        verbose=10,
                        task_type="GPU",
                        iterations=10000,
                        early_stopping_rounds=200,
                        l2_leaf_reg=3.0,
                        loss_function="MultiClass",
                        depth=6
                    )
                ))
            ],
            y_filter=lambda y: y[main_activity_label_name]
        )

    def predict_proba_pd(self, X):
        return pd.DataFrame(self.predict_proba(X), index=X.index).add_prefix("proba:various_b_").fillna(0)


class SecondaryActivityClassifier(FilteredPipeline):

    def __create_model(self):
        clf = Sequential()
        clf.add(Dropout(0.2))
        clf.add(Dense(100, activation='relu'))
        clf.add(Dropout(0.2))
        clf.add(Dense(200, activation='relu'))
        clf.add(Dropout(0.2))
        clf.add(Dense(45, activation='sigmoid'))

        clf.compile(optimizer=Adam(), loss='binary_crossentropy', metrics=['accuracy'])

        return clf;

    def __init__(self, label_names):
        super().__init__(
            steps=[
                ("impute", SimpleImputer()),
                ("standardize", StandardScaler()),
                ("classify", KerasClassifier(
                    build_fn=self.__create_model,
                    batch_size=64,
                    epochs=100
                ))
            ],
            y_filter=lambda y: y[label_names]
        )
        self.label_names = label_names

    def predict_proba_pd(self, X):
        return pd.DataFrame(self.predict_proba(X), index=X.index).add_prefix(
            "proba:secondary_activity_").fillna(0)