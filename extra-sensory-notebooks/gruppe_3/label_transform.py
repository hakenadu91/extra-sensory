def proba_to_multilabel(y, columns=None):
    """Transformiert Multilabel-Wahrscheinlichkeiten zu den entsprechenden Labeln

    :param y: DataFrame aus allen Multilabel-Columns
    :param columns: Original-Spaltennamen
    :return: data_frame
    """

    for column in y.columns:
        y[column] = y[column].round()
    y.columns = columns
    return y


def proba_to_label(y, proba_prefix):
    """Transformiert Mutual Exclusive Wahrscheinlichkeits-Spalten zu einer Spalte mit entsprechendem Index

    :param y: DataFrame aus allen für diese Operation zu berücksichtigenden Probabilities
    :param proba_prefix: Prefix vor den Wahrscheinlichkeit-Indizes
    :return: data_Frame
    """

    y = y.idxmax(axis=1)
    y = y.apply(lambda x: int(x[len(proba_prefix):]))
    return y
