import pandas as pd


class OrderEstimator:
    """Dient dazu, die Reihenfolge der Klassifizierung verschiedener Labels zu ermitteln.

    Der OrderEstimator zählt die nach "oben" und "unten" abweichenden Elemente der Korrelationsmatrix.
    Dabei werden die Elemente mit höherer Priorität gewichtet, welche eine hohe Anzahl abweichender Zellen besitzen.
    """

    def __init__(self,
                 threshold_positive=0.8,
                 threshold_negative=-0.1,
                 correlation_method="pearson"):
        self.threshold_positive = threshold_positive
        self.threshold_negative = threshold_negative
        self.correlation_method = correlation_method

    def estimate(self, y):
        correlation = y.corr(method=self.correlation_method)
        ranking = []

        for column in correlation.columns:
            relevant = []
            for row in correlation.columns:
                corr_ij = correlation[column][row]
                if corr_ij < self.threshold_negative or corr_ij > self.threshold_positive:
                    relevant.append(row)
            ranking.append((column, relevant))

        index = 1
        while index < len(ranking):
            ranking = sorted(ranking, key=lambda x: -len(x[1]))
            ref_column = ranking[index - 1][0]
            for sub_index in range(index, len(ranking)):
                sub_relevant = ranking[sub_index][1]
                if ref_column in sub_relevant:
                    sub_relevant.remove(ref_column)
            index += 1

        return ranking


class MutualExclusionDetector:
    """Erkennt sich gegenseitig ausschließende Labels

    Memory-Complexity: O(n^2)
    Baut eine Matrix auf mit M[i][j] = 1 => label[i] tritt nie gemeinsam mit label[j] auf
    """

    def detect(self, y):
        mutual_exclusion_matrix = pd.DataFrame(1, columns=y.columns, index=y.columns)
        for i in range(len(y)):
            for column in y.columns:
                if y[column][i] == 1:
                    for row in y.columns:
                        if mutual_exclusion_matrix[column][row] == 1 and y[row][i] == 1:
                            mutual_exclusion_matrix[column][row] = 0
            if i % 1000 == 0:
                print("iteration: " + str(i))
        return mutual_exclusion_matrix
