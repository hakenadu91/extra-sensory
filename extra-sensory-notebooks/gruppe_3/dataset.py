from enum import Enum

import pandas as pd
import glob
import ntpath
import re

from sklearn.preprocessing import LabelEncoder
import gruppe_3.constants as const


class InputMode(Enum):
    """Abstraktion für Übergabe des Input-Modus

    Dient der Reduzierung von Fehlern bei Nutzung des DatasetReaders
    """

    users = 0
    combined = 1


class DatasetReader:
    """Bietet Abstraktion für das Lesen von Datasets

    Der DatasetReader ermöglicht das Lesen von Pandas-DataFrames auf Basis verschiedener Input-Modi
    """

    def __init__(self, directory, mode, *args):
        """Initialisiert diese DatasetReader-Instanz

        :param directory: Zielverzeichnis
        :param mode:
            InputMode.users    - Einlesen User-Datasets aus einem Verzeichnis
            InputMode.combined - Daten werden aus User-unabhängigen Dateien eingelesen
        :param args:
            Namen der einzulesenden Dateien (ohne .csv-Suffix).
            Falls None, werden alle Dateien im Zielverzeichnis gelesen.
        """

        self.directory = directory
        self.mode = mode
        self.args = args
        self.encoders = dict()

    def __get_all_file_names(self):
        """Liefert eine Liste aller vollqualifizierten zu berücksichtigenden Dateinamen des Zielverzeichnisses

        :return: Liste aller absoluter .csv-Pfade im Zielverzeichnis
        """

        return glob.glob(self.directory + "/*.csv")

    def __get_file_names(self):
        """Liefert eine Liste aller vollqualifizierten zu berücksichtigenden Dateinamen

        :return: Liste absoluter Pfade
        """

        if self.args is None or len(self.args) == 0:
            return self.__get_all_file_names()

        file_names = []
        for arg in self.args:
            if self.mode is InputMode.users:
                file_names.append(self.directory + "/" + arg + ".features_labels.csv")
            else:
                if self.mode is InputMode.combined:
                    file_names.append(self.directory + "/" + arg)
                else:
                    raise Exception("Fehlerhafter InputModus")

        return file_names

    def __require_dataset(self):
        if self.dataset is None:
            raise Exception("Noch kein Dataset mittels read() gelesen")

    def get_all_labels(self):
        labels = []
        labels += self.get_secondary_activity_label_names()
        labels.append(const.main_activity_label_name)
        if self.get_user_id_label_name() is not None:
            labels.append(const.user_id_label_name)
        return labels

    def get_relevant_secondary_activity_label_names(self):
        secondary_activity_label_names = self.get_secondary_activity_label_names()
        return [col for col in secondary_activity_label_names
                if col not in self.secondary_activities_without_std]

    def get_secondary_activity_label_names(self):
        self.__require_dataset()
        return [col for col in self.dataset
                if col.startswith('label:')
                and not col == const.main_activity_label_name
                and not col == const.user_id_label_name]

    def get_main_activity_label_names(self):
        self.__require_dataset()
        return [col for col in self.dataset if col in const.main_activity_label_names]

    def get_user_id_label_name(self):
        self.__require_dataset()
        if not const.user_id_label_name in self.dataset:
            return None
        return const.user_id_label_name

    def __apply_missing(self, row, label_names):
        missing = True
        for label_name in label_names:
            missing = missing and (label_name not in row or (row[label_name] == 0))
        return 1.0 if missing else 0.0

    def __apply_main_activity_missing(self, row):
        return self.__apply_missing(row, const.main_activity_label_names)

    def __apply_various_a_missing(self, row):
        return self.__apply_missing(row, const.various_a_label_names)

    def __apply_various_b_missing(self, row):
        return self.__apply_missing(row, const.various_b_label_names)

    def __apply_phone_missing(self, row):
        return self.__apply_missing(row, const.phone_label_names)

    def __read_data_frame_and_append_user_id(self, X_y):
        data_frames = []
        for file_name in self.__get_file_names():
            print(file_name)
            data_frame = pd.read_csv(file_name, index_col=None, header=0)
            if X_y and self.mode is InputMode.users:
                user_id = ntpath.basename(file_name)[:-20]
                user_ids = []
                for row_index in range(0, len(data_frame)):
                    user_ids.append(user_id)
                data_frame[const.user_id_label_name] = user_ids
            data_frames.append(data_frame)
        return pd.concat(data_frames, axis=0, ignore_index=True)

    def __get_discrete_group_names(self, df):
        if hasattr(self, "discrete_group_names"):
            print("voreingestellte discrete_group_names gefunden")
            return self.discrete_group_names

        cols = [col for col in df.columns if col.startswith('discrete:')]
        group_names = []
        for col in cols:
            group_name = re.search("discrete:(.*):", col).group(1)
            if group_name not in group_names:
                group_names.append(group_name)
        return group_names

    def __create_reverted_one_hot_column(self, df, group_name):
        prefix = "discrete:" + group_name
        cols = [col for col in df.columns if col.startswith(prefix)]
        return df[cols].idxmax(axis=1)

    def __revert_one_hot_encodings(self):
        discrete_group_names = self.__get_discrete_group_names(self.dataset)
        for group_name in discrete_group_names:
            self.dataset[group_name] = self.__create_reverted_one_hot_column(self.dataset, group_name)
            self.dataset = self.dataset.drop(
                columns=[col for col in self.dataset.columns if col.startswith("discrete:" + group_name + ":")])
        self.discrete_group_names = discrete_group_names
        self.dataset = self.dataset

    def __preprocess_user_id(self):
        if const.user_id_label_name in self.dataset:
            if const.user_id_label_name not in self.encoders:
                print("Erzeuge neuen LabelEncoder für " + const.user_id_label_name)
                self.encoders[const.user_id_label_name] = LabelEncoder()
                self.encoders[const.user_id_label_name] = self.encoders[const.user_id_label_name].fit(
                    self.dataset[const.user_id_label_name])
            self.dataset[const.user_id_label_name] = self.encoders[const.user_id_label_name].transform(
                self.dataset[const.user_id_label_name])

    def __preprocess_secondary_activity(self):
        secondary_activity_label_names = self.get_secondary_activity_label_names()
        self.dataset[secondary_activity_label_names] = self.dataset[secondary_activity_label_names].fillna(0)

        std = self.dataset[secondary_activity_label_names].std()
        self.secondary_activities_without_std = std[std == 0].index

    def __aggregate_mutual_exclusive_labels(self, aggregated_label_name, label_names, missing_label_name,
                                            missing_applicator):
        print("aggregation: " + aggregated_label_name)
        extended_label_names = label_names + [missing_label_name]
        self.dataset[label_names] = self.dataset[label_names].fillna(0)
        self.dataset[missing_label_name] = self.dataset.apply(missing_applicator, axis=1)
        self.dataset[aggregated_label_name] = self.dataset[extended_label_names].idxmax(axis=1)
        self.dataset = self.dataset.drop(extended_label_names, axis=1)

        if aggregated_label_name not in self.encoders:
            print("Erzeuge neuen LabelEncoder für " + aggregated_label_name)
            self.encoders[aggregated_label_name] = LabelEncoder().fit(self.dataset[aggregated_label_name])

        self.dataset[aggregated_label_name] = self.encoders[aggregated_label_name].transform(
            self.dataset[aggregated_label_name])

    def __preprocess_main_activity(self):
        self.__aggregate_mutual_exclusive_labels(
            const.main_activity_label_name,
            const.main_activity_label_names,
            const.main_activity_missing_label_name,
            self.__apply_main_activity_missing
        )

    def __preprocess_various_a(self):
        self.__aggregate_mutual_exclusive_labels(
            const.various_a_label_name,
            const.various_a_label_names,
            const.various_a_missing_label_name,
            self.__apply_various_a_missing
        )

    def __preprocess_various_b(self):
        self.__aggregate_mutual_exclusive_labels(
            const.various_b_label_name,
            const.various_b_label_names,
            const.various_b_missing_label_name,
            self.__apply_various_b_missing
        )

    def __preprocess_phone(self):
        self.__aggregate_mutual_exclusive_labels(
            const.phone_label_name,
            const.phone_label_names,
            const.phone_missing_label_name,
            self.__apply_phone_missing
        )

    def configure(self, training_info):
        self.encoders = training_info["encoders"]
        self.discrete_group_names = training_info["discrete_group_names"]

    def read(self, X_y=True):
        """Liest einen Pandas-DataFrame auf Basis der im Konstruktor übergebenen Parameter

        :param X_y: Falls True, sind wir gerade im Training, sonst im Test
        :return: Eingelesener Pandas-DataFrame
        """

        self.dataset = self.__read_data_frame_and_append_user_id(X_y)
        self.dataset = self.dataset.drop(
            columns=[c for c in const.ignore if c in self.dataset.columns])

        if X_y:
            self.__preprocess_user_id()
            self.__preprocess_secondary_activity()
            self.__preprocess_main_activity()
            self.__preprocess_phone()
            self.__preprocess_various_a()
            self.__preprocess_various_b()

        self.__revert_one_hot_encodings()

        print(self.dataset.columns)

        if not X_y:
            return self.dataset

        labels = self.get_all_labels()
        X = self.dataset \
            .drop(columns=labels)
        y = self.dataset[labels]
        return X, y
