import sys

sys.path.append("../../")  # gruppe_3 als lib dir

import dill
import errno
import os
import pandas as pd
import re
import gruppe_3.constants as const

from gruppe_3.dataset import DatasetReader, InputMode
from gruppe_3.label_transform import proba_to_label


# ----------------------------------------------------------------------------------------
# WAHRSCHEINLICHKEITEN AUS AGGREGIERTEN FEATURES EXTRAHIEREN
# ----------------------------------------------------------------------------------------
def mkdirs(file_name):
    if not os.path.exists(os.path.dirname(file_name)):
        try:
            os.makedirs(os.path.dirname(file_name))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise


def read_output(file_name):
    print("Lese Datei: " + file_name)
    mkdirs(file_name)
    return dill.load(open(file_name, "rb"))


def extract_probas(X_with_probas, proba_prefix):
    proba_columns = [c for c in X_with_probas.columns if c.startswith(proba_prefix)]
    return X_with_probas[proba_columns]


def create_proba_prefix(name):
    return "proba:" + name + "_"


def extract_name(label_name):
    return re.search("^label:(.*)$", label_name).group(1)


def append_probabilities(X, probabilities, proba_prefix):
    X.reset_index(drop=True, inplace=True)
    probabilities.reset_index(drop=True, inplace=True)
    return pd.concat([X, probabilities], axis=1)


def perform_incremental_classification(X_test, classifier, name):
    proba_prefix = create_proba_prefix(name)

    y_test_pred_proba = classifier.predict_proba(X_test)
    y_test_pred_proba = pd.DataFrame(y_test_pred_proba, index=X_test.index).add_prefix(proba_prefix).fillna(0)
    y_pred = proba_to_label(y_test_pred_proba, proba_prefix)
    X_test = append_probabilities(X_test, y_test_pred_proba, proba_prefix)

    return X_test


def enc_transform(df, agg_label):
    if agg_label not in dataset_reader.encoders:
        return df[agg_label]
    enc = dataset_reader.encoders[agg_label]
    return enc.inverse_transform(df[agg_label])


def label_reduce(s):
    return int(s.idxmax(axis=1))


def multi_expand(s, agg_label, agg_none_label, agg_label_names):
    result = [0 for i in range(len(agg_label_names))]
    label = s[agg_label]
    if label != agg_none_label:
        idx_selected = agg_label_names.index(label)
        result[idx_selected] = 1
    return pd.Series(result)


def main_activity_expand(s):
    return multi_expand(s, const.main_activity_label_name, const.main_activity_missing_label_name,
                        const.main_activity_label_names)


def various_a_expand(s):
    return multi_expand(s, const.various_a_label_name, const.various_a_missing_label_name, const.various_a_label_names)


def various_b_expand(s):
    return multi_expand(s, const.various_b_label_name, const.various_b_missing_label_name, const.various_b_label_names)


def phone_expand(s):
    return multi_expand(s, const.phone_label_name, const.phone_missing_label_name, const.phone_label_names)


def do_inv_transform(y_out, proba_dict, agg_label, agg_label_names=None, expand=None):
    y_out[agg_label] = proba_dict[agg_label].apply(label_reduce, axis=1, result_type="reduce")
    y_out[agg_label] = enc_transform(y_out, agg_label)
    if expand is None:
        return y_out
    y_out[agg_label_names] = y_out.apply(expand, axis=1, result_type="expand")
    return y_out.drop(columns=[agg_label])


def predict():
    # Informationen aus vorangegangenem Training lesen
    training_info = read_output(estimators_output_dir + "training_info.sav")

    # DatasetReader soll mit Infos aus vorangegangenem Training arbeiten
    dataset_reader.configure(training_info)
    X_test = dataset_reader.read(X_y=False)

    discrete_group_names = training_info["discrete_group_names"]
    cat_features = [X_test.columns.get_loc(f) for f in discrete_group_names]
    num_features = [col for col in X_test.columns if col not in discrete_group_names]

    remaining_label_names = training_info["remaining_label_names"]

    # ----------------------------------------------------------------------------------------
    # Preprocessor laden und anwenden
    # ----------------------------------------------------------------------------------------
    preprocessor = read_output(estimators_output_dir + "preprocessor.sav")
    X_test[num_features] = pd.DataFrame(preprocessor.transform(X_test[num_features]),
                                        index=X_test.index,
                                        columns=num_features)

    # ----------------------------------------------------------------------------------------
    # Alle Classifier laden und der Reihe nach zur Erweiterung der Features anwenden
    # ----------------------------------------------------------------------------------------
    all_labels = const.aggregated_column_names + remaining_label_names
    for l in all_labels:
        name = extract_name(l)
        classifier = read_output(classifier_output_dir + name + "_classifier.sav")
        X_test = perform_incremental_classification(X_test, classifier, name)

    # X_test enthält zu diesem Zeitpunkt alle vorhergesagten Wahrscheinlichkeiten
    proba_dict = dict()
    for proba_name in [extract_name(l) for l in const.aggregated_column_names + remaining_label_names]:
        proba_prefix = create_proba_prefix(proba_name)
        extracted_probas = extract_probas(X_test, proba_prefix)
        extracted_probas.columns = [c[len(proba_prefix):] for c in extracted_probas.columns]
        proba_dict["label:" + proba_name] = extracted_probas

    # Einen Beispiel-Frame printen
    proba_dict["label:main_activity"].head(3)

    # ----------------------------------------------------------------------------------------
    # Pandas 23+ reduce & expand für das performante inverse Transformieren verwenden...
    # ----------------------------------------------------------------------------------------
    y_out = pd.DataFrame(index=X_test.index)

    for col_info in [
                        (const.user_id_label_name, None, None),
                        (const.main_activity_label_name, const.main_activity_label_names, main_activity_expand),
                        (const.various_a_label_name, const.various_a_label_names, various_a_expand),
                        (const.various_b_label_name, const.various_b_label_names, various_b_expand),
                        (const.phone_label_name, const.phone_label_names, phone_expand)
                    ] + [(col, None, None) for col in remaining_label_names]:
        print("inverse-transform " + col_info[0])
        y_out = do_inv_transform(y_out, proba_dict, col_info[0], col_info[1], col_info[2])

    # Aufbereitete Label in .csv-Datei schreiben
    print("Schreibe Prediction: " + output_file)
    mkdirs(output_file)
    y_out.to_csv(output_file, sep=',')


if __name__ == '__main__':
    input_dir = os.getenv('INPUT_DIR', "D:/Development/Projects/mustererkennung/data_test")
    train_output_dir = os.getenv('TRAIN_OUTPUT_DIR', "")
    output_file = os.getenv('OUTPUT_FILE', "output/prediction.csv")

    estimators_output_dir = train_output_dir + "estimators/"
    classifier_output_dir = estimators_output_dir + "classifiers/"

    print("input_dir:        " + input_dir)
    print("train_output_dir: " + train_output_dir)
    print("output_file:      " + output_file)

    dataset_reader = DatasetReader(
        input_dir,
        InputMode.users
    )

    predict()
