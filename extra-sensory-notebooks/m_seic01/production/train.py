import sys

sys.path.append("../../")  # gruppe_3 als lib dir

import dill
import errno
import os
import pandas as pd
import re
import gruppe_3.constants as const

from gruppe_3.dataset import DatasetReader, InputMode

from catboost import CatBoostClassifier, Pool

from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

input_dir = os.getenv('INPUT_DIR', os.environ.get('EXTRA_SENSORY_DATASET'))
output_dir = os.getenv('OUTPUT_DIR', "")
task_type = os.getenv('TASK_TYPE', "GPU")

estimators_output_dir = output_dir + "estimators/"
classifier_output_dir = estimators_output_dir + "classifiers/"

print("input_dir:  " + input_dir)
print("output_dir: " + output_dir)
print("task_type:  " + task_type)


# ----------------------------------------------------------------------------------------
# Definition allgemeiner Funktionen
# ----------------------------------------------------------------------------------------
def write_output(file_content, file_name):
    print("Schreibe Datei: " + file_name)
    if not os.path.exists(os.path.dirname(file_name)):
        try:
            os.makedirs(os.path.dirname(file_name))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
    dill.dump(file_content, open(file_name, "wb"))


def append_probabilities(X, probabilities, proba_prefix):
    X.reset_index(drop=True, inplace=True)
    probabilities.reset_index(drop=True, inplace=True)
    return pd.concat([X, probabilities], axis=1)


def create_proba_prefix(name):
    return "proba:" + name + "_"


def extract_name(label_name):
    return re.search("^label:(.*)$", label_name).group(1)


def perform_incremental_classification(X_train, X_val, classifier, name):
    prev_len = len(X_train.columns)

    proba_prefix = create_proba_prefix(name)

    y_train_pred_proba = classifier.predict_proba(X_train)
    y_train_pred_proba = pd.DataFrame(y_train_pred_proba, index=X_train.index).add_prefix(proba_prefix).fillna(0)
    X_train = append_probabilities(X_train, y_train_pred_proba, proba_prefix)

    y_val_pred_proba = classifier.predict_proba(X_val)
    y_val_pred_proba = pd.DataFrame(y_val_pred_proba, index=X_val.index).add_prefix(proba_prefix).fillna(0)
    X_val = append_probabilities(X_val, y_val_pred_proba, proba_prefix)

    print("Features um " + str((len(X_train.columns) - prev_len)) + " Spalten angereichert")
    return (X_train, X_val)


# ----------------------------------------------------------------------------------------
# werden die Parameter nach dem InputMode weggelassen, so werden alle Dateien im Ziel-
# verzeichnis gelesen. Letzteres wird durch eine Umgebungsvariable adressiert, damit
# alle entspannt ins Git pushen können.
# ----------------------------------------------------------------------------------------
print("Lese Dataset: " + input_dir)
dataset_reader = DatasetReader(
    input_dir,
    InputMode.users
)
X, y = dataset_reader.read()

# ----------------------------------------------------------------------------------------
# Trainings- und Validationdaten splitten
# ----------------------------------------------------------------------------------------
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42)
# Weitere Metadaten für den Catboost-Classifier festhalten
cat_features = [X.columns.get_loc(f) for f in dataset_reader.discrete_group_names]
num_features = [col for col in X.columns if col not in dataset_reader.discrete_group_names]

# ----------------------------------------------------------------------------------------
# NACH DEM STATISCHEN DATA PREPROCESSING NUN DAS DYNAMISCHE
# ----------------------------------------------------------------------------------------
preprocessor = Pipeline([
    ("impute", SimpleImputer()),
    ("scale", StandardScaler())
])

X_train[num_features] = pd.DataFrame(preprocessor.fit_transform(X_train[num_features]), index=X_train.index,
                                     columns=num_features)
X_val[num_features] = pd.DataFrame(preprocessor.transform(X_val[num_features]), index=X_val.index, columns=num_features)

write_output(preprocessor, estimators_output_dir + "preprocessor.sav")

# ----------------------------------------------------------------------------------------
# USER CLASSIFIER TRAINIEREN UND SPEICHERN
# ----------------------------------------------------------------------------------------
print("Trainiere User-Classifier")

user_classifier = CatBoostClassifier(
    task_type=task_type,
    random_state=42,
    depth=7,
    iterations=1500,
    loss_function="MultiClass",
    l2_leaf_reg=4.0,
    early_stopping_rounds=10,
    verbose=100)

user_classifier = user_classifier.fit(X_train, y_train[const.user_id_label_name],
                                      cat_features=cat_features,
                                      eval_set=Pool(X_val, y_val[const.user_id_label_name], cat_features=cat_features),
                                      verbose_eval=100)

write_output(user_classifier, classifier_output_dir + "user_classifier.sav")

# X_train und X_val um User-Probas anreichern
X_train, X_val = perform_incremental_classification(
    X_train, X_val, user_classifier, extract_name(const.user_id_label_name))

# ----------------------------------------------------------------------------------------
# MAIN ACTIVITY CLASSIFIER TRAINIEREN UND SPEICHERN
# ----------------------------------------------------------------------------------------
print("Trainiere Main-Activity-Classifier")

main_activity_classifier = CatBoostClassifier(
    task_type=task_type,
    random_state=42,
    depth=9,
    iterations=6000,
    loss_function="MultiClass",
    l2_leaf_reg=2.5,
    early_stopping_rounds=100,
    verbose=100)

main_activity_classifier = main_activity_classifier.fit(X_train, y_train[const.main_activity_label_name],
                                                        cat_features=cat_features,
                                                        eval_set=Pool(X_val, y_val[const.main_activity_label_name],
                                                                      cat_features=cat_features),
                                                        verbose_eval=100)

write_output(main_activity_classifier, classifier_output_dir + "main_activity_classifier.sav")

# X_train und X_val um Main-Activity-Probas anreichern
X_train, X_val = perform_incremental_classification(
    X_train, X_val, main_activity_classifier, extract_name(const.main_activity_label_name))

# ----------------------------------------------------------------------------------------
# VARIOUS-A CLASSIFIER TRAINIEREN UND SPEICHERN
# ----------------------------------------------------------------------------------------
print("Trainiere Various-A-Classifier")

various_a_classifier = CatBoostClassifier(
    verbose=100,
    task_type=task_type,
    random_state=42,
    iterations=5000,
    early_stopping_rounds=100,
    l2_leaf_reg=3.0,
    loss_function="MultiClass",
    depth=6
)

various_a_classifier = various_a_classifier.fit(X_train, y_train[const.various_a_label_name],
                                                cat_features=cat_features,
                                                eval_set=Pool(X_val, y_val[const.various_a_label_name],
                                                              cat_features=cat_features),
                                                verbose_eval=100)

write_output(various_a_classifier, classifier_output_dir + "various_a_classifier.sav")

# X_train und X_val um Various-A-Probas anreichern
X_train, X_val = perform_incremental_classification(
    X_train, X_val, various_a_classifier, extract_name(const.various_a_label_name))

# ----------------------------------------------------------------------------------------
# VARIOUS-B CLASSIFIER TRAINIEREN UND SPEICHERN
# ----------------------------------------------------------------------------------------
print("Trainiere Various-A-Classifier")

various_b_classifier = CatBoostClassifier(
    verbose=100,
    random_state=42,
    task_type="GPU",
    iterations=5000,
    early_stopping_rounds=100,
    l2_leaf_reg=3.0,
    loss_function="MultiClass",
    depth=6
)

various_b_classifier = various_b_classifier.fit(X_train, y_train[const.various_b_label_name],
                                                cat_features=cat_features,
                                                eval_set=Pool(X_val, y_val[const.various_b_label_name],
                                                              cat_features=cat_features),
                                                verbose_eval=100)

write_output(various_b_classifier, classifier_output_dir + "various_b_classifier.sav")

# X_train und X_val um Various-B-Probas anreichern
X_train, X_val = perform_incremental_classification(
    X_train, X_val, various_b_classifier, extract_name(const.various_b_label_name))

# ----------------------------------------------------------------------------------------
# PHONE CLASSIFIER TRAINIEREN UND SPEICHERN
# ----------------------------------------------------------------------------------------
print("Trainiere Phone-Classifier")

phone_classifier = CatBoostClassifier(
    verbose=100,
    task_type="GPU",
    iterations=5000,
    l2_leaf_reg=3.0,
    loss_function="MultiClass",
    early_stopping_rounds=100,
    depth=6,
    random_state=42
)

phone_classifier = phone_classifier.fit(X_train, y_train[const.phone_label_name],
                                        cat_features=cat_features,
                                        eval_set=Pool(X_val, y_val[const.phone_label_name], cat_features=cat_features),
                                        verbose_eval=100)

write_output(phone_classifier, classifier_output_dir + "phone_classifier.sav")

# X_train und X_val um Phone-Probas anreichern
X_train, X_val = perform_incremental_classification(
    X_train, X_val, phone_classifier, extract_name(const.phone_label_name))

# ----------------------------------------------------------------------------------------
# ALLE RESTLICHEN LABELS MIT JEWEILS EINEM MODELL TRAINIEREN UND SPEICHERN
# ----------------------------------------------------------------------------------------
remaining_label_names = [c for c in y_train.columns if c not in [
    const.user_id_label_name,
    const.main_activity_label_name,
    const.various_a_label_name,
    const.various_b_label_name,
    const.phone_label_name
]]

for remaining_label_name in remaining_label_names:
    remaining_label_name_cut = extract_name(remaining_label_name)
    remaining_classifier_file_name = classifier_output_dir + remaining_label_name_cut + "_classifier.sav"

    print("Trainiere " + remaining_label_name + "-Classifier")

    remaining_classifier = CatBoostClassifier(
        verbose=100,
        task_type="GPU",
        iterations=1200,
        depth=6,
        random_state=42,
        l2_leaf_reg=3.5,
        early_stopping_rounds=100
    )

    remaining_classifier = remaining_classifier.fit(X_train, y_train[remaining_label_name],
                                                    cat_features=cat_features,
                                                    eval_set=Pool(X_val, y_val[remaining_label_name],
                                                                  cat_features=cat_features),
                                                    verbose_eval=100)

    write_output(remaining_classifier, remaining_classifier_file_name)

    # X_train und X_val um Probas von remaining_label_name anreichern
    X_train, X_val = perform_incremental_classification(
        X_train, X_val, remaining_classifier, remaining_label_name_cut)

training_info = dict()
training_info["encoders"] = dataset_reader.encoders
training_info["discrete_group_names"] = dataset_reader.discrete_group_names
training_info["remaining_label_names"] = remaining_label_names
write_output(training_info, estimators_output_dir + "training_info.sav")

print("Training beendet. Bitte beehren Sie uns bald wieder.")
