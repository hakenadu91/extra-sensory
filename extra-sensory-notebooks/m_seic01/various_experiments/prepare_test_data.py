import glob
import pandas as pd


def __get_file_names(directory):
    return glob.glob(directory + "/*.csv")


dir = "D:/Development/Projects/mustererkennung/data_test"
for file in __get_file_names(dir):
    data_frame = pd.read_csv(file, index_col=0, header=0)
    data_frame = data_frame.drop(columns=[c for c in data_frame.columns if c.startswith("label")])
    data_frame.to_csv(file, sep=',')
