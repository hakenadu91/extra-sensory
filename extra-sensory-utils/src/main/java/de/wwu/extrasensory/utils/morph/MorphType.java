package de.wwu.extrasensory.utils.morph;

public enum MorphType {
	MAIN_ACTIVITY, SECONDARY_ACTIVITY
}
