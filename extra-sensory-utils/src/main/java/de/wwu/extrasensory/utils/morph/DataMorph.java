package de.wwu.extrasensory.utils.morph;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

public class DataMorph {

	private static final List<String> MUTUAL_EXCLUSIVE_LABELS = Stream.of(//
			"label:LYING_DOWN", //
			"label:SITTING", //
			"label:OR_standing", //
			"label:FIX_running", //
			"label:FIX_walking", //
			"label:BICYCLING"//
	).collect(Collectors.toList());

	public static void main(final String[] args) {
		if (args.length != 3) {
			throw new IllegalArgumentException(
					"Fehlerhafte Anzahl von Argumenten. 3 werden erwartet (sourceDir, targetDir, morphType), "
							+ args.length + " wurden erhalten");
		}

		final File sourceDirectory = new File(args[0]);
		if (!sourceDirectory.exists()) {
			throw new IllegalArgumentException("Source " + sourceDirectory.getAbsolutePath() + " existiert nicht");
		}
		if (!sourceDirectory.isDirectory()) {
			throw new IllegalArgumentException("Source " + sourceDirectory.getAbsolutePath() + " ist kein Verzeichnis");
		}

		final File targetDirectory = new File(args[1]);
		if (!targetDirectory.exists()) {
			targetDirectory.mkdirs();
		}
		if (!targetDirectory.isDirectory()) {
			throw new IllegalArgumentException("Target " + targetDirectory.getAbsolutePath() + " ist kein Verzeichnis");
		}

		final MorphType morphType = Optional.ofNullable(args[2]).map(String::toUpperCase).map(MorphType::valueOf)
				.orElseThrow(() -> {
					throw new IllegalArgumentException("MorphType muss MAIN_ACTIVITY oder SECONDARY_ACTIVITY sein");
				});
		Arrays.stream(sourceDirectory.listFiles())//
				.filter(file -> file.getAbsolutePath().endsWith(".csv"))//
				.forEach(file -> morph(file, targetDirectory, morphType));
	}

	private static void morph(final File file, final File targetDirectory, final MorphType morphType) {
		if (file.isDirectory()) {
			morph(file.listFiles()[0], targetDirectory, morphType);
			return;
		}
		if (file.isDirectory() || !file.getAbsolutePath().endsWith(".csv")) {
			throw new IllegalArgumentException(file.getAbsolutePath() + " ist keine .csv-Datei");
		}

		final File targetFile = new File(targetDirectory.getAbsolutePath() + '/' + file.getName());
		if (targetFile.exists()) {
			targetFile.delete();
		}

		System.out.println("morph(" + file.getName() + ") -> " + morphType);

		try (final Reader reader = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8);
				final CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());
				final Writer writer = Files.newBufferedWriter(targetFile.toPath());
				final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT)) {
			if (morphType == MorphType.MAIN_ACTIVITY) {
				morphMain(csvParser, csvPrinter);
			} else {
				morphSecondary(csvParser, csvPrinter);
			}
		} catch (final IOException ioException) {
			throw new IllegalStateException("Morph fehlgeschlagen", ioException);
		}
	}

	private static void morphMain(final CSVParser csvParser, final CSVPrinter csvPrinter) throws IOException {
		int skipped = 0;

		final Map<String, Integer> header = csvParser.getHeaderMap();
		MUTUAL_EXCLUSIVE_LABELS.forEach(header::remove);

		final List<String> newHeaders = header.keySet().stream().filter(s -> !s.startsWith("label:"))
				.collect(Collectors.toCollection(ArrayList::new));
		newHeaders.add("label:main-activity");
		csvPrinter.printRecord(newHeaders);

		for (final CSVRecord csvRecord : csvParser.getRecords()) {
			boolean printed = false;
			final List<String> newRecord = header.keySet().stream().map(csvRecord::get)
					.collect(Collectors.toCollection(LinkedList::new));
			for (int exOrdinal = 0; exOrdinal < MUTUAL_EXCLUSIVE_LABELS.size(); exOrdinal++) {
				final String mutualExclusiveLabel = MUTUAL_EXCLUSIVE_LABELS.get(exOrdinal);
				final String value = csvRecord.get(mutualExclusiveLabel);
				if ("1".equals(value)) {
					newRecord.add(String.valueOf(exOrdinal));
					csvPrinter.printRecord(newRecord);
					printed = true;
					break;
				}
			}
			if (!printed) {
				skipped++;
			}
		}

		System.out.println("Skipped: " + skipped);
	}

	private static void morphSecondary(final CSVParser csvParser, final CSVPrinter csvPrinter) throws IOException {
		final Map<String, Integer> header = csvParser.getHeaderMap();
		MUTUAL_EXCLUSIVE_LABELS.forEach(header::remove);
		header.remove("label_source");

		final List<String> newHeaders = header.keySet().stream().filter(s -> !s.startsWith("label:"))
				.collect(Collectors.toCollection(ArrayList::new));
		newHeaders.add("label_source");
		MUTUAL_EXCLUSIVE_LABELS.forEach(newHeaders::add);
		header.keySet().stream().filter(s -> s.startsWith("label:")).forEach(newHeaders::add);

		csvPrinter.printRecord(newHeaders);

		for (final CSVRecord csvRecord : csvParser.getRecords()) {
			final List<String> newRecord = newHeaders.stream().map(csvRecord::get)
					.collect(Collectors.toCollection(LinkedList::new));
			csvPrinter.printRecord(newRecord);
		}
	}
}
