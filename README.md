Pattern Recognition Practical Project
=====================================

Classification Experiments based on the 
[ExtraSensory Dataset](http://extrasensory.ucsd.edu/)

Authors
-------
* Manuel De Ceglia
* Sagad Hamid
* Tayfun Honluk
* Manuel Seiche

Project Structure
-----------------
* extra-sensory-core
    * Core Functionality (including train.py and predict.py)
* extra-sensory-rest
    * Tornado REST API for accessing the extra-sensory-core functionality
* extra-sensory-notebooks
    * Jupyter Notebooks (Not used in production)
* extra-sensory-utils
    * Java Utilities for external Preprocessing (Not used in production)

Related Work
------------
* [Vaizman, Y., Ellis, K., and Lanckriet, G. 
"Recognizing Detailed Human Context In-the-Wild from Smartphones and 
Smartwatches". IEEE Pervasive Computing, vol. 16, no. 4, October-December 2017, 
pp. 62-74. doi:10.1109/MPRV.2017.3971131](https://ieeexplore.ieee.org/document/8090454)