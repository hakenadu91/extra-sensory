import sys
import logging
import pandas as pd

from imblearn.pipeline import Pipeline
from imblearn.over_sampling import SMOTE
from sklearn.preprocessing import Imputer, StandardScaler
from sklearn.cluster import FeatureAgglomeration
from sklearn.model_selection import train_test_split
from catboost import CatBoostClassifier

import shared

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)

main_pipeline = None
secondary_pipeline = None


def create_main_pipeline(dataset):
    main_pipeline = Pipeline([
        ('impute', Imputer()),
        ('oversample', SMOTE(random_state=42)),
        ('scale', StandardScaler()),
        ('cluster', FeatureAgglomeration(n_clusters=90)),
        ('classify', CatBoostClassifier(
            task_type="GPU",
            depth=10,
            random_state=42,
            verbose=100
        ))
    ])


def read_dataframe(dataset):
    data_frame = pd.read_csv(dataset, index_col=None, header=0)


def read_arguments(args):
    """extracts the required command line arguments

    :param args: command line arguments ("predict.py", <dataset>, <classifier>, <output>)
    :return: (dataset, classifier, output)
    """

    len_args = len(args)
    if len_args != 4:
        raise ValueError("expected 4 arguments but got " + str(len_args))
    return args[1], args[2], args[3]


if __name__ == '__main__':
    dataset_directory, classifier, output = read_arguments(sys.argv)
    logging.info(
        "got arguments dataset='{0}', classifier='{1}', output='{2}'".format(dataset_directory, classifier, output))
