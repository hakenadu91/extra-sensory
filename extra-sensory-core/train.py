import sys
import shared
import pickle

from imblearn.pipeline import Pipeline
from imblearn.over_sampling import SMOTE
from sklearn.preprocessing import Imputer, StandardScaler
from catboost import CatBoostClassifier
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.cluster import FeatureAgglomeration
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from skmultilearn.problem_transform import LabelPowerset

import numpy as np


class PipelineChain(BaseEstimator, ClassifierMixin):

    def __init__(self, pipelines):
        self.pipelines = pipelines

    def fit(self, X, y):
        return self

    def predict(self, X):
        predictions = []
        latest_prediction = None
        for pipeline in self.pipelines:
            if latest_prediction is not None:
                X = np.concatenate((X, latest_prediction), axis=1)
            prediction = pipeline.predict(X)
            predictions.append(prediction)
            latest_prediction = prediction.reshape(-1, 1)
        return predictions


def train_main_pipeline(data_frame):
    X_main, y_main = shared.split_X_y_main(data_frame)
    main_pipeline = Pipeline([
        ('impute', Imputer()),
        ('oversample', SMOTE(random_state=42)),
        ('scale', StandardScaler()),
        ('cluster', FeatureAgglomeration(n_clusters=90)),
        ('classify', LabelPowerset(
            RandomForestClassifier(
                random_state=42,
                verbose=True
            )
            #CatBoostClassifier(
            #    task_type="GPU",
            #    depth=10,
            #    random_state=42,
            #    verbose=10,
            #    iterations=50
            #)
        ))
    ])
    main_pipeline.fit(X_main, y_main)
    return main_pipeline


def train_secondary_pipeline(data_frame):
    X_secondary, y_secondary = shared.split_X_y_secondary(data_frame)
    secondary_pipeline = Pipeline([
        ('impute', Imputer()),
        ('scale', StandardScaler()),
        ('oversample', SMOTE(random_state=42)),
        ('decomposition', PCA(.95)),
        ('classify', LabelPowerset(
            RandomForestClassifier(
                random_state=42,
                verbose=True
            )
            #CatBoostClassifier(
            #    task_type="GPU",
            #    depth=6,
            #    random_state=42,
            #    verbose=10,
            #    iterations=50
            #)
        ))
    ])
    secondary_pipeline.fit(X_secondary, y_secondary)
    return secondary_pipeline


def read_arguments(args):
    """extracts the required command line arguments

    :param args: command line arguments ("train.py", <dataset>, <classifier>)
    :return: (dataset, classifier)
    """

    len_args = len(args)
    if len_args != 3:
        raise ValueError("expected 3 arguments but got " + str(len_args))
    return args[1], args[2]


if __name__ == '__main__':
    # dataset_directory, classifier = read_arguments(sys.argv)

    dataset_directory = 'D:/Development/Projects/mustererkennung/data'
    classifier = 'classifier.sav'
    output = 'output.sav'

    data_frame = shared.read_data_frame(dataset_directory)

    main_pipeline = train_main_pipeline(data_frame)
    secondary_pipeline = train_secondary_pipeline(data_frame)

    combined_pipeline = PipelineChain([
        main_pipeline,
        secondary_pipeline
    ])

    pickle.dump(combined_pipeline, open('wine-pipeline.sav', 'wb'))
