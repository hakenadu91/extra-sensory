import os
import glob
import ntpath
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelEncoder
from skmultilearn.problem_transform import LabelPowerset
import pickle

ignore = [
    "timestamp",
    "label_source"
]
main_activities = [
    "label:LYING_DOWN",
    "label:SITTING",
    "label:OR_standing",
    "label:FIX_running",
    "label:FIX_walking",
    "label:BICYCLING"
]
secondary_activities = [
    "label:SLEEPING",
    "label:LAB_WORK",
    "label:IN_CLASS",
    "label:IN_A_MEETING",
    "label:LOC_main_workplace",
    "label:OR_indoors",
    "label:OR_outside",
    "label:IN_A_CAR",
    "label:ON_A_BUS",
    "label:DRIVE_-_I_M_THE_DRIVER",
    "label:DRIVE_-_I_M_A_PASSENGER",
    "label:LOC_home",
    "label:FIX_restaurant",
    "label:PHONE_IN_POCKET",
    "label:OR_exercise",
    "label:COOKING",
    "label:SHOPPING",
    "label:STROLLING",
    "label:DRINKING__ALCOHOL_",
    "label:BATHING_-_SHOWER",
    "label:CLEANING",
    "label:DOING_LAUNDRY",
    "label:WASHING_DISHES",
    "label:WATCHING_TV",
    "label:SURFING_THE_INTERNET",
    "label:AT_A_PARTY",
    "label:AT_A_BAR",
    "label:LOC_beach",
    "label:SINGING",
    "label:TALKING",
    "label:COMPUTER_WORK",
    "label:EATING",
    "label:TOILET",
    "label:GROOMING",
    "label:DRESSING",
    "label:AT_THE_GYM",
    "label:STAIRS_-_GOING_UP",
    "label:STAIRS_-_GOING_DOWN",
    "label:ELEVATOR",
    "label:AT_SCHOOL",
    "label:PHONE_IN_HAND",
    "label:PHONE_IN_BAG",
    "label:PHONE_ON_TABLE",
    "label:WITH_CO-WORKERS",
    "label:WITH_FRIENDS",
]

user_id_label_name = "label:user_id"


def read_data_frame(dataset_directory):
    all_files = glob.glob(dataset_directory + "/*.csv")

    data_frames = []
    for file_name in all_files:
        data_frame = pd.read_csv(file_name, index_col=None, header=0)

        user_ids = []
        user_id = ntpath.basename(file_name)[:-20]
        for row_index in range(0, len(data_frame)):
            user_ids.append(user_id)
        data_frame[user_id_label_name] = user_ids

        data_frames.append(data_frame)

    dataset = pd.concat(data_frames, axis=0, ignore_index=True)
    dataset = dataset.drop(columns=ignore)
    dataset = dataset.drop(columns=[user_id_label_name])
    return dataset


def read_pipeline(save_file):
    if os.path.exists(save_file):
        return pickle.load(open(save_file, 'rb'))
    else:
        return None


def split_X_y(data_frame, label_columns):
    y = data_frame[label_columns].fillna(0)
    X = data_frame.drop(columns=label_columns)
    return X.as_matrix(), y.as_matrix()


def split_X_y_main(data_frame):
    return split_X_y(data_frame.drop(columns=secondary_activities)
                     .dropna(subset=main_activities, how='all'), main_activities)


def split_X_y_secondary(data_frame):
    return split_X_y(data_frame.dropna(subset=secondary_activities, how='all'), secondary_activities)
